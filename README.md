## ivermectin

`ivermectin` is a script to handle your WORMs (backups) (pun intended). It can be used to clean
old backups from restic or borg backup repositories, free space and verify data in the backup repository.

**`ivermectin`** does not save sensitive data like unencrypted ssh-keys, BORG keys or the restic passwords 
anywhere, instead it uses the environment variable IVERMECTIN_MAGIC or an interactively entered password to
decrypt the data that resides in the configuration file.


## The Challenge

The challenge to WORMs is: the backup client can backup to the server at any time.
However, since the backup repository is Write Once Read Many, it is not possible to do a cleanup:
the backup client cannot delete old backups which are no longer necessary.

**ivermectin** to the rescue: ivermectin logs in to your backup server, locks the account (so your
backup client cannot access the repository - this avoids inconsistencies), cleans up the repository
and finally restores the configuration on the backup server again.

For a WORM based setup with borg see "Append-only mode" in the online documentation at
https://borgbackup.readthedocs.io/en/stable/usage/notes.html

restic needs to be combined with rclone to provide that functionality, a search engine will help you
with the setup.


## Setup

ivermectin needs a configuration file. This files can contain any number of sections, where each
section represents a backup repository. The sections are processed serially, so ivermectin can
clean repositories of many backup clients in one batch job. A DEFAULT section can be used with
default parameters (e.g. if all your backups are kept on the same server using the same ssh
port).

On the backup client you usually use a ssh-key which has no password protection, so your backups run without
any user interaction, but for ivermectin you should create a shiny new ed25519 ssh key with password protection.


### Serverside configuration

Put the new ed25519 ssh key in the file ~/.ssh/authorized_keys of the user of the backup server.
On the server the respective user also needs write access to its own ~/.ssh/authorized_keys to lock the repository,
so the appropriate access rights must be set on that file and it must not contain command restrictions for the
ivermectin ssh-key.


### Configuration file

The configuration is a simple ini file, with a section for each job. The user is expected to have
ONE PASSWORD for everything that is encrypted within the whole config file, or to rephrase it:
all ssh-keys, all BORG_KEYs or all strings to encrypt the restic password need to be the same.
You can set this password in the environment variable IVERMECTIN_MAGIC, or, in case this variable does
not exist the program will ask you for the password interactively.


- sshuser: the user to be used
- server: the server to connect to
- port: the port on the server
- location: the location on the server.
- backup_secret: either the BORG_KEY or the encrypted restic password
- command: the command to issue on the server
- ssh_privkey: the ssh private key (only ed25519 supported)
- ssh_pubkey: the ssh public key


**location**: ./repo represents a relative path, ~/repo represents a relative path to the home directory of the user.
Both ./ and ~/ will be stripped of the location because ivermectin uses sftp and sftp defaults to using the home directory.

**backup_secret**: either the content of your BORG_KEY file located at ~/.config/borg/keys/server__location. Use 
```borg key change-passphrase sftp://user@server:repo``` to adapt the password. Or the ENCRYPTED password of your restic
repository. Use ```python generate-restic-secret.py ``` to encrypt that password and paste the restulting string in the
config file

**ssh_privkey**: your private ssh key (ed25519), protected with a password. Usually the contents of ~/.ssh/id_ed25519.
You can change the password using ```ssh-keygen -p -f ~/.ssh/id_ed25519```

### Installation

It is recommended to use Python's virtual environments:

1. Create a virtual environment called ivermectin-venv ```python3 -m venv ivermectin-venv```
2. Enter the venv and install dependencies ```source ivermectin-venv/bin/activate```
3. Clone this repo ```git clone https://gitlab.com/rob2g2/ivermectin```
4. Install dependencies ```pip install -r ivermectin/requirements.txt```
5. Run ivermectin ```python3 ivermectin/ivermectin.py config.ini```
6. Exit the venv ```deactivate```


## Issues

ivermectin uses sftp and pyinvoke, so your ~/.ssh/config might cause problems or your invoke configuration

When you want some more debug messages, in ivermectin.py change line number 11 to 

logging.basicConfig(format="%(asctime)s - %(message)s", level=logging.DEBUG)
